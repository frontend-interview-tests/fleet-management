const pool = require('./db');

const app = async (url, token) => {
  const parts = `${url}`.split('/')
  const lat = parts.pop()
  const long = parts.pop()
  const id = parts.pop()
  const newToken = `T${Math.random()}T`
  const conn = await pool.getConnection()
  if(!id || !long || !lat){
    throw Error(404)  
  }

  const [[oldRow]] = await conn.execute('select token from vehicle where id = ? limit 1;',[id]) 
  if(oldRow && (!token || token !== oldRow.token)){
    throw Error(!token ? 401 : 403)
  }

  const [newRow] = await conn.execute(
    'insert into vehicle(id,name,token) VALUES (?,?,?) ON DUPLICATE KEY UPDATE token=VALUES(token);',
    [id, 'name' ,newToken]
  )
  if(newRow.affectedRows === 1){
    console.log('registered new vehicle')
  } else if(newRow.affectedRows === 2){
    console.log('updated token')
  } else {
    throw Error(500)
  }
  const [log] = await conn.execute(
    `insert into vehicle_log (vehicle_id,coords) values (?,Point(?,?))`, 
    [id, long, lat]
  )
  if(log.affectedRows !== 1){
    throw Error(422)
  }
  conn.release()
  return newToken;
};

module.exports = (req, res) => {
  const parts = `${req.url}`.split('/')
  const lat = parts.pop()
  const long = parts.pop()
  const id = parts.pop()
  const newToken = `T${Math.random()}T`
  return pool.execute('select token from vehicle where id = ? limit 1;',[id])
    .then(([[row]])=>{
      // console.log('success:'+row.token)
      res.writeHead(200, { 'Content-Type': 'text/html' })
      res.write('token');
      res.end();
    })
    .catch((e)=>{
      console.log('failed::',e.message)
      res.end();
    })
}