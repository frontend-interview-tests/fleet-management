// V1
const pool = require("./db");

async function resetVehicle(){
  await pool.query(`CREATE TABLE vehicle(
    id INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    token VARCHAR(255)
    ) ENGINE=InnoDB;
  `)
}

async function resetLog(){
  await pool.query(`CREATE TABLE vehicle_log(
    id INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    vehicle_id INT(10) UNSIGNED NOT NULL,
    coords POINT NOT NULL,
    SPATIAL INDEX \`SPATIAL\` (\`coords\`),
    INDEX vehicle_id (vehicle_id),
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(id) ON DELETE CASCADE
    ) ENGINE=InnoDB;
  `)
}

async function drop(){
  await pool.query("DROP TABLE IF EXISTS vehicle_log");
  await pool.query("DROP TABLE IF EXISTS vehicle;");
}

//////////////////// END v1 //////////////

console.log('starting...')
drop()
  .catch((e)=>console.log('droping tables failed',e))
  .then(()=>resetVehicle())  
  .then(()=>console.log('vehicle reset'))
  .catch((e)=>console.log('vehicle reset FAILED',e))
  .then(()=>resetLog())
  .then(()=>console.log('log reset'))
  .catch((e)=>console.log('log reset FAILED', e))
  .finally(()=>process.exit(0))