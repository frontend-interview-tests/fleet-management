const cluster = require('cluster');
const http = require('http');
const app = require('./app');
const numCPUs = require('os').cpus().length;

function run(){
  const server = http.createServer(app)
  server.listen(process.env.PORT || 3700);

  server.on('error', (e) => {
    console.log('ERROR:'+e.code)
    console.error(e)
  });
  console.log(`Worker ${process.pid} started`);
}

if(process.env.NODE_ENV !== 'production'){
  // return run();
}

if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    if (worker.isDead()) {
      console.info(`'worker dead pid ${worker.process.pid} signal: ${signal} code: ${code}`)
    }
    // cluster.fork()
  }); 
} else {
  run();
}