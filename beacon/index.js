const fetch = require('node-fetch');	//npm install node-fetch

function getRandomInRange(from, to, fixed=0) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
}
let c = [0,0,0,0]
function run(){
  const id = getRandomInRange(100, 100000, 0)

  function ping(token=csrfToken){
    const long = getRandomInRange(-180, 180, 3);
    const lat = getRandomInRange(-180, 180, 3);
    
    ++c[1]
    fetch(
      `http://${process.env.SENSOR_URL || 'localhost'}:3700/${id}/${long}/${lat}`, 
      {
      method: 'GET',
      headers: { 'x-token': `${token}` },
    })
    .then(res => res.text())
    .then(token => {
      if(!token || token[0] !== 'T'){
        throw Error('invalid csrfToken recieved from server')
      }
      ++c[3]
      setTimeout(()=>ping(token),500);
    })
    .catch(e=>{
      console.log('Failed::.. stoping #'+id, e)
      ++c[2]
    })
  }
  setTimeout(ping,500);
}

let i = Number(process.argv[2]||'')
if(Number.isNaN(i)){
  i = 10;
}

async function bootstrap(){
  while(i-- > 0){
    await new Promise(resolve=>setTimeout(()=>resolve(), getRandomInRange(1000,4000)))
    run() 
  }
}